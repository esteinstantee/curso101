@extends('layouts.dashboard')

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-4">
            <h1>Lista de users</h1>
            <a class="btn btn-block btn-primary" href="{{ url('user/create') }}">Crear user</a>            
            <!--<a class="btn btn-block btn-primary" href="{{ route('user.create') }}">Crear2 user</a>-->
          </div>
          <div class="col-sm-4">
            <!--<a target="_blank" class="btn btn-block btn-secondary" href="{{ url('user/reporte') }}">Reporte</a>            

            <a target="_blank" class="btn btn-block btn-secondary" href="{{ url('user/reporte/excel') }}">Reporte Excel</a>            
            -->
            <!--<ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Buttons</li>
            </ol>
            -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Responsive Hover Table</h3>

                <div class="card-tools">
                  <form name="form1" method="POST" action="{{ url('user/filtro') }}">
                    <div class="input-group input-group-sm" style="width: 350px;">

                        @csrf
                        <input type="text" name="filtro_name" class="form-control float-right" placeholder="nombre" value="{{ Session::get('filtro_name')}}">
                        <input type="text" name="filtro_apellidos" class="form-control float-right" placeholder="apellidos" value="{{ Session::get('filtro_apellidos')}}">
                      
                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                      
                    </div>
                  </form>

                  <form name="form_limpiar" method="POST" action="{{ url('user/filtro/clear') }}">
                        @csrf
                        <button type="submit" class="btn btn-default">
                          Limpiar
                        </button>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>                      
                      <th>Nombre</th>
                      <th>Apellidos</th>
                      <th>Profesion</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}<br>
                          {{ $user->created_at->format('d-m-Y H:i:s') }}

                          @php
                            $fecha=\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->created_at);                            
                          @endphp
                          <br>
                          {{$fecha->day."-".$fecha->month."-".$fecha->year}}

                          <br>
                          {{ \Carbon\Carbon::parse($user->created_at)->format('d-m-Y') }}
                          </td> 
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->apellidos }}</td>
                        <td>
                          
                          @php
                            $profesion=App\Models\Profesion::where('id_profesion',$user->id_profesion)->first();
                          @endphp
                            {{ $profesion->nombre }}<br>
                     
                        </td>
                        <td><a class="btn btn-block btn-secondary" href="{{ url('user/edit/'.$user->id) }}">editar</a></td>
                        <td><a class="btn btn-block btn-danger" href="{{ url('user/delete/'.$user->id) }}">eliminar</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

              
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    {{ $users->links() }}
    <br>
    {{ $users->count() }}
    <br>
    {{ $users->total() }}

@endsection





