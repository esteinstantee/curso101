@extends('layouts.dashboard')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-4">
          <h1>Usuario:Crear</h1>
          <a class="btn btn-block btn-success" href="{{ url('user/index') }}">Volver</a>                      
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    </div>
</section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">

          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Datos del nuevo usuario</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form name="form1" method="POST" action="{{ url('user/create/store') }}" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="name">Nombre:</label>
                  <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="name" required />
                </div>                
                <div class="form-group">
                  <label for="apellidos">Apellidos:</label>
                  <input type="text" class="form-control" id="apellidos" name="apellidos" value="{{ old('apellidos') }}" placeholder="apellidos" required />
                </div>  
                <div class="form-group">
                  <label for="email">Email:</label>
                  <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="email" required />
                </div> 
                <div class="form-group">
                  <label for="password">Password:</label>
                  <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" placeholder="password" required />
                </div> 
                <div class="form-group">
                  <label for="id_profesion">Profesion:</label>

                  <select class="form-control" id="id_profesion" name="id_profesion">
                    @foreach($profesiones as $profesion)
                      <option value="{{$profesion->id_profesion}}">{{$profesion->nombre}}</option>                    
                    @endforeach
                  </select>
                </div> 
                <div class="form-group">
                  <label for="foto">foto:</label>
                  <input type="file" class="form-control" id="foto" name="foto" value="{{ old('foto') }}" placeholder="foto" required />
                </div> 
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
            </form>
          </div>
          <!-- /.card -->

        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>






@endsection