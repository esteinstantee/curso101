@extends('layouts.dashboard')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-4">
          <h1>Profesion:Crear</h1>
          <a class="btn btn-block btn-success" href="{{ url('profesion/index') }}">Volver</a>                      
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    </div>
</section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">

          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Datos de la nueva profesión</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form name="form1" method="POST" action="{{ url('profesion/create/store') }}">
              @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="nombre">Profesion:</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre') }}" placeholder="nombre" required />
                </div>                
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
            </form>
          </div>
          <!-- /.card -->

        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>






@endsection