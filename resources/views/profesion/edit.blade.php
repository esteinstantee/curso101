@extends('layouts.dashboard')

@section('content')

    @foreach($errors->all() as $error)
        {{ $error }}
    @endforeach

    <form name="form1" method="POST" action="{{ url('profesion/edit/store') }}">
        @csrf
        {{ method_field('PUT') }}
        Profesion:
        <input type="hidden" name="id_profesion" value="{{$profesion->id_profesion}}" />
        <input type="text" name="nombre" value="{{ old('nombre',$profesion->nombre) }}" required />
        <br>
        <input type="submit" value="Guardar" />
    </form>

@endsection