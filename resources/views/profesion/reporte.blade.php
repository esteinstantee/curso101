

<img style="width: 60px; heigth: 60px;" src="{{asset('dashboard/img/avatar.png')}}">

<table style="border-collapse: collapse;border:1px;border-color:#664422;width:800px;">
      <tr>
        <th style="border-collapse: collapse;border:1px;border-color:#664422;width:200px;">ID</th>                      
        <th style="border-collapse: collapse;border:1px;border-color:#664422;width:200px;">Nombre</th>
        <th style="border-collapse: collapse;border:1px;border-color:#664422;width:200px;">Usuarios</th>
      </tr>
      @foreach($profesiones as $profesion)
      <tr>
          <td style="border-collapse: collapse;border:1px;border-color:#664422;width:200px;">{{ $profesion->id_profesion }}<br>
            {{ $profesion->created_at->format('d-m-Y H:i:s') }}

            @php
              $fecha=\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$profesion->created_at);                            
            @endphp
            <br>
            {{$fecha->day."-".$fecha->month."-".$fecha->year}}

            <br>
            {{ \Carbon\Carbon::parse($profesion->created_at)->format('d-m-Y') }}
            </td> 
          <td style="border-collapse: collapse;border:1px;border-color:#664422;width:200px;">{{ $profesion->nombre }}</td>
          <td style="border-collapse: collapse;border:1px;border-color:#664422;width:200px;">
            <!--
            @php
              $users=App\Models\User::where('id_profesion',$profesion->id_profesion)->get();
            @endphp
            @foreach($users as $usuario)
              {{ $usuario->name }}<br>
            @endforeach
            -->
            
            @foreach(($profesion->users) as $usuario)
              {{ $usuario->name }}<br>
            @endforeach
          </td>
      </tr>
      @endforeach
  </table>