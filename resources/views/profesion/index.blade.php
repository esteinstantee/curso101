@extends('layouts.dashboard')

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-4">
            <h1>Lista de profesiones</h1>
            <a class="btn btn-block btn-primary" href="{{ url('profesion/create') }}">Crear profesion</a>            
            <!--<a class="btn btn-block btn-primary" href="{{ route('profesion.create') }}">Crear2 profesion</a>-->
          </div>
          <div class="col-sm-4">
            <a target="_blank" class="btn btn-block btn-secondary" href="{{ url('profesion/reporte') }}">Reporte</a>            

            <a target="_blank" class="btn btn-block btn-secondary" href="{{ url('profesion/reporte/excel') }}">Reporte Excel</a>            
            <!--<ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Buttons</li>
            </ol>
            -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Responsive Hover Table</h3>

                <div class="card-tools">
                  <form name="form1" method="POST" action="{{ url('profesion/filtro') }}">
                    <div class="input-group input-group-sm" style="width: 350px;">

                        @csrf
                        <input type="text" name="filtro_nombre" class="form-control float-right" placeholder="profesion" value="{{ Session::get('filtro_nombre')}}">
                        <input type="text" name="filtro_name" class="form-control float-right" placeholder="name" value="{{ Session::get('filtro_name')}}">
                      
                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                      
                    </div>
                  </form>

                  <form name="form_limpiar" method="POST" action="{{ url('profesion/filtro/clear') }}">
                        @csrf
                        <button type="submit" class="btn btn-default">
                          Limpiar
                        </button>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>                      
                      <th>Nombre</th>
                      <th>Usuarios</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($profesiones as $profesion)
                    <tr>
                        <td>{{ $profesion->id_profesion }}<br>
                          {{ $profesion->created_at->format('d-m-Y H:i:s') }}

                          @php
                            $fecha=\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$profesion->created_at);                            
                          @endphp
                          <br>
                          {{$fecha->day."-".$fecha->month."-".$fecha->year}}

                          <br>
                          {{ \Carbon\Carbon::parse($profesion->created_at)->format('d-m-Y') }}
                          </td> 
                        <td>{{ $profesion->nombre }}</td>
                        <td>
                          <!--
                          @php
                            $users=App\Models\User::where('id_profesion',$profesion->id_profesion)->get();
                          @endphp
                          @foreach($users as $usuario)
                            {{ $usuario->name }}<br>
                          @endforeach
                          -->
                          
                          @foreach(($profesion->users) as $usuario)
                            {{ $usuario->name }}<br>
                          @endforeach
                        </td>
                        <td><a class="btn btn-block btn-secondary" href="{{ url('profesion/edit/'.$profesion->id_profesion) }}">editar</a></td>
                        <td><a class="btn btn-block btn-danger" href="{{ url('profesion/delete/'.$profesion->id_profesion) }}">eliminar</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

              
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    {{ $profesiones->links() }}
    <br>
    {{ $profesiones->count() }}
    <br>
    {{ $profesiones->total() }}

@endsection





