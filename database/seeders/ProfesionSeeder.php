<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Profesion;

class ProfesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profesion::create([
            "id_profesion"=>'1',
            "nombre"=>'Informático'
        ]);
        Profesion::create([
            "id_profesion"=>'2',
            "nombre"=>'Administrador'
        ]);
        Profesion::create([
            "id_profesion"=>'3',
            "nombre"=>'Arquitecto'
        ]);
        Profesion::create([
            "id_profesion"=>'4',
            "nombre"=>'Ingeniero'
        ]);

        Profesion::factory(10)->create();

        for($i=0;$i<100;$i++){
            Profesion::create([
                "nombre"=>'Ingeniero'
            ]);
        }
        
    }
}
