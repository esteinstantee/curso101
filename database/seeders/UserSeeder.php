<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Rolando',
            'apellidos'=>'Quispe',
            //'ci'=>'4578899',
            'email'=>'rolandex4545@gmail.com',
            'password'=>bcrypt('123456'),
            'id_profesion'=>1,
            ]);
        User::create([
            'name'=>'Juan',
            'apellidos'=>'Perez',
            //'ci'=>'952334',
            'email'=>'juan1234@gmail.com',
            'password'=>bcrypt('abcdef'),
            'id_profesion'=>2,
            ]);
        User::create([
                'name'=>'Maria',
                'apellidos'=>'Tapia',
                //'ci'=>'952334',
                'email'=>'maria1234@gmail.com',
                'password'=>bcrypt('abcdef'),
                'id_profesion'=>2,
                ]);
    }
}
