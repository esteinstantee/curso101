<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect()->route('loginper.ingreso');    
});

Route::get('/saludo/{nombre?}', function ($nombre=null) {
    if($nombre){
        return "Hola ".$nombre;
    } else {
        return "Hola desconocido";
    } 
});

Route::get('/usuarios', function () {
    return "Lista de usuarios";
});

Route::get('/usuario/{id_usuario}', function ($id_usuario) {
    return "detalle del usuario".$id_usuario;
})->where('id_usuario','[0-9]+');




Route::group(['middleware' => 'admin'], function () {

    //-----------rutas profesiones

    //nota en laravel 8 exige ruta al controlador sea absoluta

    Route::get('profesion/index','App\Http\Controllers\ProfesionController@index')->name('profesion.index');

    Route::get('profesion/create','App\Http\Controllers\ProfesionController@create')->name('profesion.create');
    Route::post('profesion/create/store','App\Http\Controllers\ProfesionController@createStore')->name('profesion.createstore');

    //Al modicar se puede usar post, put
    Route::get('profesion/edit/{id_profesion}','App\Http\Controllers\ProfesionController@edit')->name('profesion.edit')->where('id_profesion','[0-9]+');
    //Route::post('profesion/edit/store','App\Http\Controllers\ProfesionController@editStore')->name('profesion.editstore');
    Route::put('profesion/edit/store','App\Http\Controllers\ProfesionController@editStore')->name('profesion.editstore');

    Route::get('profesion/delete/{id_profesion}','App\Http\Controllers\ProfesionController@delete')->name('profesion.delete')->where('id_profesion','[0-9]+');

    Route::post('profesion/filtro','App\Http\Controllers\ProfesionController@filtro')->name('profesion.filtro');
    Route::post('profesion/filtro/clear','App\Http\Controllers\ProfesionController@filtroClear')->name('profesion.filtroclear');

    Route::get('profesion/reporte','App\Http\Controllers\ProfesionController@reporte')->name('profesion.reporte');
    Route::get('profesion/reporte/excel','App\Http\Controllers\ProfesionController@reporteExcel')->name('profesion.reporte.excel');


    //Usuarios
    Route::get('user/index','App\Http\Controllers\UserController@index')->name('user.index');

    Route::get('user/create','App\Http\Controllers\UserController@create')->name('user.create');
    Route::post('user/create/store','App\Http\Controllers\UserController@createStore')->name('user.createstore');

    //Al modicar se puede usar post, put
    Route::get('user/edit/{id}','App\Http\Controllers\UserController@edit')->name('user.edit')->where('id','[0-9]+');
    //Route::post('user/edit/store','App\Http\Controllers\UserController@editStore')->name('user.editstore');
    Route::put('user/edit/store','App\Http\Controllers\UserController@editStore')->name('user.editstore');

    Route::get('user/delete/{id}','App\Http\Controllers\UserController@delete')->name('user.delete')->where('id','[0-9]+');

    Route::post('user/filtro','App\Http\Controllers\UserController@filtro')->name('user.filtro');
    Route::post('user/filtro/clear','App\Http\Controllers\UserController@filtroClear')->name('user.filtroclear');

});

Route::get('loginper/ingreso','App\Http\Controllers\LoginController@ingreso')->name('loginper.ingreso');
Route::post('loginper/validar','App\Http\Controllers\LoginController@validar')->name('loginper.validar');
Route::get('loginper/terminar','App\Http\Controllers\LoginController@terminar')->name('loginper.terminar');
