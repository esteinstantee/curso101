<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function ingreso(){
        return view('loginper.ingreso');
    }
    public function validar(){
        $usuario=request()->input('usuario');
        $password=request()->input('password');
        //Validar en la tabla de users
        //VAlido ir al administrador de profesiones

        $usuario=\App\Models\User::where('email',$usuario)->first();

        if($usuario!=null){
            if(Hash::check($password,$usuario->password)){
            //if($datos['password']==$usuario->password){
                Session::put('vg_sesion_iniciada','1');
                return redirect()->route('profesion.index');
            }
        }        

        //no valido retornar al login
        return redirect()->route('loginper.ingreso');
    }

    public function terminar(){
        Session::flush();
        return redirect()->route('loginper.ingreso');
    }
}
