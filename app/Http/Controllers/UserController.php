<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profesion;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    public function filtro(){
        $filtro_name=request()->input("filtro_name");
        $filtro_apellidos=request()->input("filtro_apellidos");
        Session::put('filtro_name',$filtro_name);
        Session::put('filtro_apellidos',$filtro_apellidos);
        return redirect()->route('user.index');
    }
    public function filtroClear(){        
        Session::put('filtro_name',"");
        Session::put('filtro_apellidos',"");
        return redirect()->route('user.index');
    }

    public function index(){
        //obtener todas las users
        //select * from users
        
        //$users=User::get();//User::all();

        
        $pageuser="page_user";
        if(!Session::has($pageuser)){
            Session::put($pageuser,'1');
        }
        if(request($pageuser,0)!==0)Session::put($pageuser,request($pageuser,0));

        $paginaActual=(int)Session::get($pageuser);

        \Illuminate\Pagination\Paginator::currentPageResolver(function() use ($paginaActual) {
            return $paginaActual;
        });

        //construir condiciones
        $condiciones=[];
        if(Session::has('filtro_name')){
            $filtro_name=Session::get('filtro_name');
            if (strlen($filtro_name)>0) {
                $condiciones[]=['users.name','like',"%".$filtro_name."%"];
            }
        }
        
        if(Session::has('filtro_apellidos')){
            $filtro_apellidos=Session::get('filtro_apellidos');
            if (strlen($filtro_apellidos)>0) {
                $condiciones[]=['users.apellidos','like',"%".$filtro_apellidos."%"];

            }
            
        }

            $users=User::where($condiciones)
            ->orderBy('id','asc')
            ->paginate(20);//User::all();
        

        $users->setPageName('page_user');

        if($paginaActual>($users->lastPage())){
            return redirect($users->url(1));
        }

        //dd($users);
        //print_r($users);
        //exit(0);

        return view('user.index',[
            'users'=>$users,
        ]);
    }

    //Mostrar formulario crear
    public function create(){
        $profesiones=Profesion::get();
        return view('user.create',[
            'profesiones'=>$profesiones,
        ]);
    }
    
    //Recepcionando los datos del formulario con el request UserRequest
    public function createStore(UserRequest $request){
        $datos=$request->validationData();
        $datos['password']=bcrypt($datos['password']);


        //--------subir archivo
        $foto=request()->file('foto');
        if($foto){
            //mime, tam, caracteres especilaes: acentos  
            $codunico=User::count()+1;
            $arch_foto=$foto->getClientOriginalName();
            $nuevo_nombre=$codunico.'_'.$arch_foto;
            $datos['foto']=$nuevo_nombre;
            //disco personalizado publico
            Storage::disk('discopublico')->putFileAs('fotos/',$foto,$nuevo_nombre);

            //Storage::disk('public')->putFileAs('fotos/',$foto,$nuevo_nombre);


        }


        //dd($datos);
        $user=User::create($datos);

        return redirect()->route('user.index');
    }

    public function edit($id){
        $user=User::find($id);

        return view('user.edit',[
            'profesion'=>$profesion,
        ]);
    }
    
    //Recepcionando los datos del formulario con el request UserRequest
    public function editStore(UserRequest $request){
        $datos=$request->validationData();
        
        $id=request()->input('id');
        //dd($id);
        $user=User::find($id);
        if($profesion!=null){
            $profesion->update($datos);
        }
        

        return redirect()->route('user.index');
    }

    //Eliminacion directa
    public function delete($id){
        $user=User::find($id);

        $profesion->delete();

        return redirect()->route('user.index');
    }
}
