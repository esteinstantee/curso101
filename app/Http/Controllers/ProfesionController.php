<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profesion;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ProfesionRequest;

use Spipu\Html2Pdf\Html2Pdf;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ProfesionController extends Controller
{
    public function filtro(){
        $filtro_nombre=request()->input("filtro_nombre");
        $filtro_name=request()->input("filtro_name");
        Session::put('filtro_nombre',$filtro_nombre);
        Session::put('filtro_name',$filtro_name);
        return redirect()->route('profesion.index');
    }
    public function filtroClear(){        
        Session::put('filtro_nombre',"");
        Session::put('filtro_name',"");
        return redirect()->route('profesion.index');
    }

    public function index(){
        //obtener todas las profesiones
        //select * from profesiones
        
        //$profesiones=Profesion::get();//Profesion::all();

        
        $pagename="page_profesion";
        if(!Session::has($pagename)){
            Session::put($pagename,'1');
        }
        if(request($pagename,0)!==0)Session::put($pagename,request($pagename,0));

        $paginaActual=(int)Session::get($pagename);

        \Illuminate\Pagination\Paginator::currentPageResolver(function() use ($paginaActual) {
            return $paginaActual;
        });

        //construir condiciones
        $condiciones=[];
        if(Session::has('filtro_nombre')){
            $filtro_nombre=Session::get('filtro_nombre');
            if (strlen($filtro_nombre)>0) {
                $condiciones[]=['profesiones.nombre','like',"%".$filtro_nombre."%"];
            }
        }
        $sw=0;
        if(Session::has('filtro_name')){
            $filtro_name=Session::get('filtro_name');
            if (strlen($filtro_name)>0) {
                $condiciones[]=['users.name','like',"%".$filtro_name."%"];
                $sw=1;
            }
            
        }

        if ($sw==0) {
            $profesiones=Profesion::where($condiciones)
            ->orderBy('id_profesion','asc')
            ->paginate(20);//Profesion::all();
        } else {
            $profesiones=Profesion::join("users", "users.id_profesion", "=", "profesiones.id_profesion")
            ->select('profesiones.*', 'users.name')
            ->where($condiciones)
            //->orderBy('profesiones.id_profesion','asc')
            ->paginate(20);
        }

        $profesiones->setPageName('page_profesion');

        if($paginaActual>($profesiones->lastPage())){
            return redirect($profesiones->url(1));
        }

        //dd($profesiones);
        //print_r($profesiones);
        //exit(0);

        return view('profesion.index',[
            'profesiones'=>$profesiones,
        ]);
    }

    //Mostrar formulario crear
    public function create(){
        return view('profesion.create');
    }
    
    //Recepcionando los datos del formulario con el request ProfesionRequest
    public function createStore(ProfesionRequest $request){
        $datos=$request->validationData();
        //dd($datos);
        $profesion=Profesion::create($datos);

        return redirect()->route('profesion.index');
    }

    public function edit($id_profesion){
        $profesion=Profesion::find($id_profesion);

        return view('profesion.edit',[
            'profesion'=>$profesion,
        ]);
    }
    
    //Recepcionando los datos del formulario con el request ProfesionRequest
    public function editStore(ProfesionRequest $request){
        $datos=$request->validationData();
        
        $id_profesion=request()->input('id_profesion');
        //dd($id_profesion);
        $profesion=Profesion::find($id_profesion);
        if($profesion!=null){
            $profesion->update($datos);
        }
        

        return redirect()->route('profesion.index');
    }

    //Eliminacion directa
    public function delete($id_profesion){
        $profesion=Profesion::find($id_profesion);

        $profesion->delete();

        return redirect()->route('profesion.index');
    }


    public function reporte(){
        $profesiones=Profesion::all();

        //P,L
        //LEGAL,A4
        $html2pdf = new HTML2PDF('P', 'A4', 'es', true, 'UTF-8', array(15,20,15,20));
        $html2pdf->setDefaultFont('Arial');
        
        $html2pdf->writeHTML(view('profesion.reporte',[
            'profesiones'=>$profesiones,
            ]));
        $html2pdf->output();

        //return view('profesiones.reporte',['profesiones'=>$profesiones]);
    }

    public function reporteExcel(){
        $profesiones=Profesion::all();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getCell('A1')->setValue('Nro');
        $sheet->getCell('B1')->setValue('Profesion');
        $n=2;
        foreach($profesiones as $prof){
            $sheet->getCell('A'.$n)->setValue($n-1);
            $sheet->getCell('B'.$n)->setValue($prof->nombre);
            $n=$n+1;
        }
        $spreadsheet->getActiveSheet()->setTitle('hojareporte1');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //make it an attachment so we can define filename
        header('Content-Disposition: attachment;filename="profesionesrep'.'.xlsx"');
        
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
 
    }
}
