<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Profesion extends Model
{
    use HasFactory;

    protected $primaryKey="id_profesion";//si no especifican, entonces laravel asume que se llama id
    protected $table="profesiones";//si no especifican, se asume el plural del modelo

    protected $fillable=[
        'nombre',
    ];

    public function users(){
        return $this->hasMany('App\Models\User', 'id_profesion');
    }

    public function users2(){
        return User::where('id_profesion',$this->id_profesion)->get();
    }
        

}
